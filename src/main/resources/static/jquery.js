$( document ).ready(function() {
	
	// SUBMIT FORM
    $("#empFormData").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});
    
    
    function ajaxPost(){
    	
    	// PREPARE FORM DATA
    	var formData = {
    			ename : $("#ename").val(),
    			esalary :  $("#esalary").val()
    	}
    	$.ajax({
			type : "POST",
			contentType : "application/json",
			url : window.location + "api/emp/save",
			data : JSON.stringify(formData),
			dataType : 'json'
				)};
    }