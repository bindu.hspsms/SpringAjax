package com.bindu.empajaxcrud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomePage {

	@GetMapping(value="/index")
	public String indexPage()
	{
		return "index";
	}
	
}
