package com.bindu.empajaxcrud.controller;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bindu.empajaxcrud.emprepository.EmpRepository;
import com.bindu.empajaxcrud.model.Emp;

@RestController
@RequestMapping("/api")
public class EmpController {

	@Autowired
	EmpRepository empRepository;
	
	
	@PostMapping(value="/emp/save")
	public Emp saveEmpData(@RequestBody Emp e)
	{
		empRepository.save(e);
		return e;
		
	}
}
