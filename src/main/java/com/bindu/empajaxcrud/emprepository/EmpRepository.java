package com.bindu.empajaxcrud.emprepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bindu.empajaxcrud.model.Emp;

@Repository
public interface EmpRepository extends JpaRepository<Emp, Long> {

}
