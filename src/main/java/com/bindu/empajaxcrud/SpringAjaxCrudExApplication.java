package com.bindu.empajaxcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringAjaxCrudExApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAjaxCrudExApplication.class, args);
	}
}
